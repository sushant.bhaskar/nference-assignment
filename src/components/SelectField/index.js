import React from "react";
import "./index.css";
import Menu from "../Menu";

const SelectField = ({ onChange, menuItems, onSelect, selected, menuState, ...rest }) => {
  return (
    <div className="search-container">
      <div className="search-input" {...rest}>
        Please Select
        <svg className="down-arrow" focusable="false" viewBox="0 0 24 24" aria-hidden="true">
          <path d="M7 10l5 5 5-5z"></path>
        </svg>
      </div>

      <Menu menuItems={menuItems} onSelect={onSelect} selected={selected} menuState={menuState} />
    </div>
  );
};

export default SelectField;

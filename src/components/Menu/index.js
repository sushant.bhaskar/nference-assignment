import React from "react";
import Checkbox from "../Checkbox";
import "./index.css";

const Menu = ({ menuItems, selected, onSelect, menuState }) => {
  return (
    <div style={menuState ? { transform: `scale(1)` } : { transform: `scale(0)` }} className="menu-paper">
      {menuItems.map((menuItem, ind) => {
        return (
          <Checkbox
            key={menuItem}
            label={menuItem}
            checked={selected.indexOf(menuItem) > -1}
            onChange={(e) => onSelect(e, menuItem)}
          />
        );
      })}
    </div>
  );
};

export default Menu;

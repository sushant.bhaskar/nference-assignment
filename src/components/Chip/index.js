import React from "react";
import CrossIcon from "../CrossIcon";
import "./index.css";

const Chip = ({ label, onClose }) => {
  return (
    <div className="chip-container">
      <span>{label}</span>
      <CrossIcon onClick={onClose} className="chip-icon" width="22" height="22" />
    </div>
  );
};

export default Chip;

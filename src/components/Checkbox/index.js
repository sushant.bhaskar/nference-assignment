import React from "react";
import "./index.css";

const Checkbox = ({ label, ...rest }) => {
  return (
    <div className="menuItem-cont">
      <label> {label}</label>
      <input type="checkbox" value={label} {...rest} />
    </div>
  );
};

export default Checkbox;

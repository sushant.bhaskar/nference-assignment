import React, { Component } from "react";
import "./App.css";
import { Chip, SelectField } from "./components";

const menuItems = ["Jane Doe", "John Doe", "Stevie Feliciano", "Chris Evans", "Robert Downey", "Scarlett Johansson"];

class App extends Component {
  constructor(props) {
    super(props);
    this.wrapperRef = React.createRef();
    this.state = {
      selected: [],
      menuState: false,
    };
  }

  componentDidMount = () => {
    document.addEventListener("mousedown", this.handleClickOutside);
  };

  componentWillUnmount = () => {
    document.removeEventListener("mousedown", this.handleClickOutside);
  };

  handleClickOutside = (event) => {
    if (
      this.wrapperRef &&
      !this.wrapperRef.current.contains(event.target) &&
      event.target.tagName !== "svg" &&
      event.target.tagName !== "path"
    ) {
      this.closeMenu();
    }
  };

  handleSelection = (event, item) => {
    let { selected } = this.state;
    let selectedCopy = selected.slice();
    let clickedIndex = selectedCopy.indexOf(item);
    if (event.target.checked) {
      selectedCopy = [...selected, item];
    } else {
      selectedCopy = [...selected.slice(0, clickedIndex), ...selected.slice(clickedIndex + 1)];
    }
    this.setState({ selected: selectedCopy });
  };

  toggleMenu = () => {
    let { menuState } = this.state;
    this.setState({ menuState: !menuState });
  };

  closeMenu = () => {
    this.setState({ menuState: false });
  };

  render() {
    const { selected, menuState } = this.state;
    return (
      <div className="App">
        <h3>Nference Assignment</h3>
        <div className="select-field-container" ref={this.wrapperRef}>
          <SelectField
            menuItems={menuItems}
            onSelect={this.handleSelection}
            selected={selected}
            onClick={this.toggleMenu}
            menuState={menuState}
          />
        </div>
        <div className="chip-wrapper">
          {selected.map((item) => {
            return (
              <Chip
                key={item}
                label={item}
                onClose={() => this.handleSelection({ target: { checked: false } }, item)}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

export default App;
